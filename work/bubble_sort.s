// bubble-sort.S file template, rename and implement the algorithm
// Test algorithm in qtmips_gui program
// Select the CPU core configuration with delay-slot
// This setups requires (for simplicity) one NOP instruction after
// each branch and jump instruction (more during lecture about pipelining)
// The code will be compiled and tested by external mips-elf-gcc
// compiler by teachers, you can try make in addition, but testing
// by internal assembler should be enough

// copy directory with the project to your repository to
// the directory work/bubble-sort
// critical is location of the file work/bubble-sort/bubble-sort.S
// which is checked by the scripts

// Directives to make interesting windows visible
#pragma qtmips show registers
#pragma qtmips show memory

.set noreorder
.set noat

.globl    array_size
.globl    array_start

.text
.globl _start
.ent _start

_start:

	la   $a0, array_start
	la   $a1, array_size
	lw   $a1, 0($a1) // number of elements in the array

//Insert your code there
outer_loop:
	addiu $a2, $a0, 0 //temporary start
	addiu $a3, $a1, -1 //temporary size

	inner_loop:
		lw   $t0, 0($a2)
		lw   $t1, 4($a2)
		sub  $t2, $t1, $t0
		bgtz $t2, next_step
		nop
		sw $t1, 0($a2)
		sw $t0, 4($a2)
	
		next_step:
			addi $a2, $a2, 4
			addi $a3, $a3, -1
			bgtz $a3, inner_loop
			nop
	addiu $a1, $a1, -1 //the last position of cycle is settled, leave it be
	bgtz $a1, outer_loop
	nop
//Final infinite loop
end_loop:
	cache 9, 0($0)  // flush cache memory
	break           // stop the simulator
	j end_loop
	nop

.end _start

.data
// .align    2 // not supported by QtMips yet

array_size:
.word	15
array_start:
.word	5, 3, 4, 1, 15, 8, 9, 2, 10, 6, 11, 1, 6, 9, 12

// Specify location to show in memory window
#pragma qtmips focus memory array_size
